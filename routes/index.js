
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var Handlebars=require('handlebars');
var delay = require('delay');
var url = 'mongodb://less:less2020@ds125932.mlab.com:25932/mydb';
var express = require('express');
var router = express.Router();
var fs = require('fs');

var moment=require('moment');

var multer = require('multer');
var path=require('path');

var storage = multer.diskStorage({
    destination: function (req, file, next) {
        next(null, './public/uploads/');
    },
    filename: function (req, file, next) {
        next(null, file.fieldname + '-' + Date.now() + '.jpg')
    }
});

var upload = multer({ storage: storage }).single('pic');


var employee_ID="";
var fileName;

var gender=['male','female','other'];
var blood=['A+','B+','AB+','O+','A-','B-','AB-','O-'];
var marital=['married','unmarried'];
var DOC=['Debit','Credit'];
var totalEmployee=0;
/* GET home page. */
var Balance=0;
var catName="";
var current_ID="";
var pageID="";

module.exports = function(app, passport) {

    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', isLoggedIn, function(req, res) {

        res.redirect('/view');
    });


    app.get('/view',isLoggedIn, function(req, res, next) {

        var debit=0,credit=0;
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("Category").find({}).toArray(function (err, result) {
                dbo.collection("DataTable").find({}).toArray(function (err, result2) {
                    for (var i = 0; i < result2.length; i++) {
                        if (result2[i].DOC === 'Debit') {
                            debit += parseInt(result2[i].Amount,10);
                        }
                        else if(result2[i].DOC === 'Credit'){
                            credit += parseInt(result2[i].Amount,10);
                        }
                        else{

                        }
                    }
                    Balance = credit - debit;
                    console.log(result2);
                    res.render('view', {Categories: result,array:result2,Balance:Balance,layout:'accountLayout'});
                    db.close();
                });
            });


        });
    });
    // =====================================
    // LOGIN ===============================
    // =====================================
    // show the login form
    app.get('/login', function(req, res) {

        // render the page and pass in any flash data if it exists
        res.render('login', { message: req.flash('loginMessage') });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/view', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    // =====================================
    // SIGNUP ==============================
    // =====================================
    // show the signup form
    app.get('/signup', function(req, res) {

        // render the page and pass in any flash data if it exists
        console.log("here");
        console.log(req.user)
        if(req.user!==undefined){
            res.render('signup', { message: "",layout:'layout', user:req.user.local });

        }
        else
        {
            res.render('signup', { message: ""});
        }

    });

    //process the signup form
    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/view', // redirect to the secure profile section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        session: false,
        failureFlash : true // allow flash messages

    }));
    // =====================================
    // PROFILE SECTION =========================
    // =====================================
    // we will want this protected so you have to be logged in to visit
    // we will use route middleware to verify this (the isLoggedIn function)


    // =====================================
    // LOGOUT ==============================
    // =====================================
    app.get('/logout', function(req, res) {
        req.logout();
        // res.clearCookie('connect.sid');
        res.redirect('/');
    });
    app.post('/saveTable', isLoggedIn, function(req, res, next) {

        upload(req, res, function (err) {
            if (err) {
                // An error occurred when uploading
            }
            if(req.file){
                var result=req.file;

                fileName=result.filename;
                console.log(fileName);
            }

        });
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            var Category=req.body.Category;
            //console.log(Category);
            if(Category==="") {
                Category = "Undefined";
            }
            dbo.collection("Category").find({name:Category}).toArray(function (err, result2) {
                if (result2.length === 0) {
                    dbo.collection("Category").insertOne({
                        name: Category
                    }, function (err, res2) {
                        if (err) throw err;
                        console.log("1 document inserted");
                    });
                }
                dbo.collection("DataTable").find({}).toArray(function (err, result) {
                    var existing = result;
                    if(existing.length!==0){
                        var ID = (parseInt(existing[existing.length-1]._id,10) + 1).toString();
                    }
                    else{
                        ID="1";
                    }
                    dbo.collection("DataTable").insertOne({
                            _id: ID,
                            date: moment.utc(req.body.date, "DD/MM/YYYY").toDate(),
                            Description: req.body.Description,
                            Amount: req.body.Amount,
                            DOC: req.body.DOC,
                            Category: Category,
                            Responsible: req.body.Responsible,
                            imgFile: fileName
                        }, function (err, res2) {
                            if (err) throw err;
                            fileName = '';
                            console.log("1 document inserted");
                            res.redirect('/');

                        }
                    );
                });
            });
        });

    });

    app.post('/editTable', isLoggedIn, function(req, res, next) {
        var referer=req.headers.referer;
        referer=referer.split('/');
        pageID=referer[referer.length-1];
        console.log(pageID);
        current_ID=req.body.ID;
        console.log(current_ID);
        res.redirect('/editTable');

    });
    app.get('/editTable',isLoggedIn,function(req, res, next) {
        mongo.connect(url, function(err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("Category").find({}).toArray(function (err, result2) {
                dbo.collection("DataTable").find({_id:current_ID}).toArray(function(err, result) {
                    if (err) throw err;
                    var result1=result[0];
                    console.log(result1);
                    if (result1.Category==="Undefined"){
                        result1.Category="";
                    }
                    result1.date=moment(result1.date).format("DD/MM/YYYY");
                    res.render('editTable',{array:result1,Categories:result2,layout:'accountLayout'});
                    db.close();
                });
            });

        });
    });
    app.post('/editSave', isLoggedIn,function(req, res, next) {
        upload(req, res, function (err) {
            if (err) {
                // An error occurred when uploading
            }
            if(req.file) {
                var result = req.file;

                fileName = result.filename;
                console.log(fileName);
                mongo.connect(url, function (err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    var Category=req.body.Category;
                    //console.log(Category);
                    if(Category==="") {
                        Category = "Undefined";
                    }
                    dbo.collection("Category").find({name: Category}).toArray(function (err, result2) {
                        if (result2.length === 0) {
                            dbo.collection("Category").insertOne({
                                name: Category
                            }, function (err, res2) {
                                if (err) throw err;
                                console.log("1 document inserted");
                            });
                        }
                        var myquery = {_id: current_ID};
                        var newvalues = {
                            $set: {
                                date: moment.utc(req.body.date, "DD/MM/YYYY").toDate(),
                                Description: req.body.Description,
                                Amount: req.body.Amount,
                                DOC: req.body.DOC,
                                Responsible:req.body.Responsible,
                                Category: Category,
                                imgFile: fileName
                            }
                        };
                        dbo.collection("DataTable").updateOne(myquery, newvalues, function (err, res2) {
                            if (err) throw err;
                            console.log("Data updated");
                            if (pageID==="view"){
                                res.redirect('/');
                            }
                            else{
                                res.redirect('/viewDetails');
                            }

                            db.close();
                        });
                    });
                });
            }
            else{
                mongo.connect(url, function (err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    var Category=req.body.Category;
                    //console.log(Category);
                    if(Category==="") {
                        Category = "Undefined";
                    }
                    dbo.collection("Category").find({name: Category}).toArray(function (err, result2) {
                        if (result2.length === 0) {
                            dbo.collection("Category").insertOne({
                                name: Category
                            }, function (err, res2) {
                                if (err) throw err;
                                console.log("1 document inserted");
                            });
                        }
                        var myquery = {_id: current_ID};
                        var newvalues = {
                            $set: {
                                date: moment.utc(req.body.date, "DD/MM/YYYY").toDate(),
                                Description: req.body.Description,
                                Amount: req.body.Amount,
                                DOC: req.body.DOC,
                                Responsible:req.body.Responsible,
                                Category: Category
                            }
                        };
                        dbo.collection("DataTable").updateOne(myquery, newvalues, function (err, res2) {
                            if (err) throw err;
                            console.log("Data updated");
                            if (pageID==="view"){
                                res.redirect('/');
                            }
                            else{
                                res.redirect('/viewDetails');
                            }
                            db.close();
                        });
                    });
                });

            }
        });

    });
    app.post('/deleteData',isLoggedIn, function(req, res, next) {
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            var myquery = { _id: current_ID };
            dbo.collection("DataTable").deleteOne(myquery, function(err, obj) {
                if (err) throw err;
                console.log("1 document deleted");
                db.close();
                res.redirect('/');
            });


        });
    });
    app.get('/viewCategory',isLoggedIn,function(req, res, next) {
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("Category").find({}).toArray(function (err, result2) {
                res.render('viewCategory',{array:result2,layout:'accountLayout'});
                db.close();

            });

        });
    });

    app.post('/deleteCategory',isLoggedIn,function(req, res, next) {
        var deleteID=req.body.deleteID;
        console.log(deleteID);
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            var myquery = {name: deleteID};
            if (deleteID==="null"){
                deleteID=JSON.parse(deleteID);
            }
            console.log(deleteID);
            dbo.collection("Category").deleteOne(myquery, function (err, obj) {
                if (err) throw err;
                console.log("1 document deleted from Category");
                dbo.collection("Category").find({name: "Undefined"}).toArray(function (err, result2) {
                    if (result2.length == 0) {
                        dbo.collection("Category").insertOne({
                            name: "Undefined"
                        }, function (err, res2) {
                            if (err) throw err;
                            console.log("1 document inserted in Category");
                        });
                    }

                    var myquery = {Category: deleteID};
                    var newvalues = {
                        $set: {
                            Category: "Undefined"
                        }
                    };
                    dbo.collection("DataTable").updateMany(myquery, newvalues, function (err, res2) {
                        if (err) throw err;
                        console.log("CategoryName updated in DataTable");
                        res.redirect('/viewCategory');
                        db.close();
                    });
                });


            });

        });

    });
    app.post('/editCategory',isLoggedIn,function(req, res, next) {
        var editedName=req.body.editedName;
        var prevName=req.body.editID;
        console.log(editedName);
        console.log(prevName);
        if(editedName===prevName){
            res.redirect('/viewCategory');
        }
        else{
            mongo.connect(url, function (err, db) {
                if (err) throw err;
                var dbo = db.db("mydb");
                if(editedName===null || editedName==="" ){
                    editedName="Undefined";
                }
                var myquery = {name: prevName};
                var newvalues = {
                    $set: {
                        name: editedName
                    }
                };
                dbo.collection("Category").find({name: editedName}).toArray(function (err, result2) {

                    if (result2.length==0){
                        dbo.collection("Category").updateOne(myquery, newvalues, function (err, res2) {
                            if (err) throw err;
                            console.log("CategoryName updated");
                        });
                    }
                    else {
                        myquery = {name: prevName};
                        dbo.collection("Category").deleteOne(myquery, function (err, obj) {
                            if (err) throw err;
                            console.log("1 document deleted from Category");
                        });
                    }
                    myquery = {Category: prevName};
                    newvalues = {
                        $set: {
                            Category: editedName
                        }
                    };
                    dbo.collection("DataTable").updateMany(myquery, newvalues, function (err, res2) {
                        if (err) throw err;
                        console.log("CategoryName updated in DataTable");
                        res.redirect('/viewCategory');
                        db.close();
                    });
                });
            });
        }



    });


    app.post('/viewDetails',isLoggedIn,function (req,res,next) {
        catName=req.body.ID;
        console.log(catName);
        res.redirect('/viewDetails');



    });
    app.get('/viewDetails',isLoggedIn,function (req,res,next) {
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("DataTable").find({Category: catName}).toArray(function (err, result2) {
                if (err) throw err;
                console.log(result2);
                res.render('viewDetails', {array: result2,name:catName,layout:'accountLayout'});
                db.close();
            });

        });


    });

    app.post('/deleteDataFromCategory',isLoggedIn,function (req,res,next) {
        var id=req.body.deleteID;
        console.log(id);
        var ids=id.split(",");
        console.log(ids);
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            for (var i=0;i<ids.length;i++) {
                var myquery = {_id: ids[i]};
                dbo.collection("DataTable").deleteOne(myquery, function (err, obj) {
                    if (err) throw err;
                    console.log("1 document deleted");

                });
            }
            db.close();
            res.redirect('/viewDetails');

        });



    });

    app.get('/demo',function (req,res,next) {
        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("DataTable").find({}).toArray(function (err, result2){
                console.log(result2);
                result2.map(function (item) {
                    var date = moment.utc(item.date, "DD/MM/YYYY");
                    dbo.collection("DataTable").updateOne({_id: item._id}, {$set: {date: date.toDate()}})
                });
            });
        });
    });



    app.get('/export',isLoggedIn,function (req,res,next) {

        mongo.connect(url, function (err, db) {
            if (err) throw err;
            var dbo = db.db("mydb");
            dbo.collection("Category").find({}).toArray(function (err, result2) {
                if (err) throw err;
                console.log(result2);
                var arrayDummy=[];
                res.render('report', {array2: arrayDummy,array: result2, layout: 'accountLayout'});
                db.close();

            });
        });

    });
    app.post('/generateReport',isLoggedIn,function (req,res,next) {
        var date=req.body.date;
        console.log(date);
        var month=req.body.attendMonth;
        console.log(month);
        var Category=req.body.Category;
        console.log(Category);
        var catHead="Category:"+Category;
        if (date !== ""){
            var head="Date:"+date;
            date=moment.utc(req.body.date, "DD/MM/YYYY").toDate();
            if (Category==="All"){
                mongo.connect(url, function(err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        dbo.collection("DataTable").find({date: date}).toArray(function (err, result) {
                            console.log(result);
                            res.render('report', {array2: result,head:head, Category:catHead,array: result2, layout: 'accountLayout'});
                            db.close();
                        });
                    });
                });
            }
            else{
                mongo.connect(url, function(err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        dbo.collection("DataTable").find({date: date,Category:Category}).toArray(function (err, result) {
                            console.log(result);
                         /*   result.forEach(function (item) {
                                item["date"]=moment(item.date).format("DD/MM/YYYY");
                            });*/
                            res.render('report', {array2: result,head:head,Category:catHead, array: result2, layout: 'accountLayout'});
                            db.close();
                        });
                    });
                });
            }

        }
        else if (month !== ""){
            month=month.split('/');
            console.log(month[0]);
            var year=month[1];
            console.log(year);
            var head=moment(month[0]).format("MMMM").toString();
            console.log(head);
            head="Month:"+head+','+month[1];
            if (Category==="All"){

                mongo.connect(url, function(err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        db.collection("DataTable").find({
                            "$and":
                                [{"$expr": {"$eq": [{"$month": "$date"}, parseInt(month[0])]}},
                                    {"$expr": {"$eq": [{"$year": "$date"}, parseInt(year)]}},
                                   ]
                        })
                            .toArray(function (err, result) {
                            console.log(result);
                            /*result.forEach(function (item) {
                                item["date"]=moment(item.date).format("DD/MM/YYYY");
                            });*/
                            res.render('report',{array2: result,head:head,Category:catHead,  array: result2, layout: 'accountLayout'});
                            db.close();
                        });
                    });
                });
            }
            else{
                mongo.connect(url, function(err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        db.collection("DataTable").find({
                            "$and":
                                [{"$expr": {"$eq": [{"$month": "$date"}, parseInt(month[0])]}},
                                    {"$expr": {"$eq": [{"$year": "$date"}, parseInt(year)]}},
                                    {Category:Category}]
                        })
                            .toArray(function (err, result) {
                                console.log(result);
                                /*result.forEach(function (item) {
                                    item["date"]=moment(item.date).format("DD/MM/YYYY");
                                });*/
                                res.render('report',{array2: result, head:head, Category:catHead,array: result2, layout: 'accountLayout'});
                                db.close();
                            });
                    });
                });
            }

        }
        else{
            var start=moment.utc(req.body.start, "DD/MM/YYYY").toDate();
            var end=moment.utc(req.body.end, "DD/MM/YYYY").toDate();
            var head="Range: "+req.body.start+" TO "+req.body.end;
            if (Category==="All") {
                mongo.connect(url, function (err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        db.collection("DataTable").find({
                            "$and":
                                [{date: {$gte: start}},
                                    {date: {$lte: end}}]
                        })
                            .toArray(function (err, result) {
                                console.log(result);
                             /*   result.forEach(function (item) {
                                    item["date"] = moment(item.date).format("DD/MM/YYYY");
                                });*/
                                res.render('report', {array2: result, head:head,Category:catHead,array: result2, layout: 'accountLayout'});
                                db.close();
                            });
                    });
                });
            }
            else {
                mongo.connect(url, function (err, db) {
                    if (err) throw err;
                    var dbo = db.db("mydb");
                    dbo.collection("Category").find({}).toArray(function (err, result2) {
                        db.collection("DataTable").find({
                            "$and":
                                [{date: {$gte: start}},
                                    {date: {$lte: end}},
                                    {Category:Category}]
                        })
                            .toArray(function (err, result) {
                                console.log(result);
                                /*result.forEach(function (item) {
                                    item["date"] = moment(item.date).format("DD/MM/YYYY");
                                });*/
                                res.render('report', {array2: result,head:head,Category:catHead, array: result2, layout: 'accountLayout'});
                                db.close();
                            });
                    });
                });
            }
        }

    });




};

// route middleware to make sure
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
}





/*router.get('/', function(req, res, next) {
    var debit=0,credit=0;
    mongo.connect(url, function (err, db) {
        if (err) throw err;
        var dbo = db.db("mydb");
        dbo.collection("Category").find({}).toArray(function (err, result) {
            dbo.collection("DataTable").find({}).toArray(function (err, result2) {
                for (var i = 0; i < result2.length; i++) {
                    if (result2[i].DOC == 'Debit') {
                        debit += parseInt(result2[i].Amount,10);
                    }
                    else if(result2[i].DOC == 'Credit'){
                        credit += parseInt(result2[i].Amount,10);
                    }
                    else{

                    }
                }
                Balance = credit - debit;
                res.render('view', {Categories: result,array:result2,Balance:Balance});
                db.close();
            });
        });


    });
});*/

Handlebars.registerHelper('ShowTable',function(array) {
    var string="";
    for(var i=0;i<array.length;i++){
        string=string+"<tr>" +
            "<td>"+array[i]._id+"</td>"+
            "<td>"+moment(array[i].date).format("DD/MM/YYYY")+"</td>"+
            "<td>"+array[i].Description+"</td>"+
            "<td>"+array[i].Amount+"</td>"+
            "<td>"+array[i].DOC+"</td>"+
            "<td>"+array[i].Responsible+"</td>"+
            "<td>"+array[i].Category+"</td>"+
        "</tr>";
    }
    return new Handlebars.SafeString(string);
});
Handlebars.registerHelper('CreateCategory',function(array) {
    var string="";
    for(var i=0;i<array.length;i++){
        if(array[i].name!=="Undefined"){
            string=string+"<option value='"+array[i].name+"'>"+array[i].name+"</option>";
        }

    }
    return new Handlebars.SafeString(string);
});






Handlebars.registerHelper('editDOC',function (data) {
    bg="";
    for(var i=0;i<DOC.length;i++){
        if(DOC[i]==data){
            bg = bg + "<option value=\""+DOC[i]+"\" selected>" + DOC[i]+ "</option>\n";
        }
        else{
            bg = bg + "<option value=\""+DOC[i]+"\">" + DOC[i]+ "</option>\n";
        }
    }
    return new Handlebars.SafeString(bg);
});


Handlebars.registerHelper('CategoryList',function (array) {
    string="";
    for(var i=0;i<array.length;i++) {
        string=string+"<tr>" +
            "<td>"+(i+1)+"</td>\n"+
            "<td class='nm'>"+array[i].name+"</td>\n"+
            "<td>" +
            "<button type='submit' id='showDetails' name='ID' value=\""+array[i].name+"\" class=\"btn btn-default btn-sm\" ><span class=\"glyphicon glyphicon-th-list\"></span></button>\n" +
            "<button type=\"button\" id='editBtn'"+i+" class=\"edit btn btn-default btn-sm\" data-toggle=\"modal\"  data-id=\""+array[i].name+"\" data-target=\"#editModal\"> <span class=\"glyphicon glyphicon-edit\"></span> Edit</button>\n" +
            "<button type=\"button\" id='deleteBtn' class=\"delete btn btn-default btn-sm\"  data-toggle=\"modal\"  data-id=\""+array[i].name+"\" data-target=\"#confirm-delete\"><span class=\"glyphicon glyphicon-trash\"></span> Delete </button>\n" +
            "</td>\n"+
        "</tr>";
    }

    return new Handlebars.SafeString(string);
});
Handlebars.registerHelper('ReportDetailsRow',function (array) {
    string="";
    var debit=0,credit=0;
    for (var i = 0; i < array.length; i++) {
        if (array[i].DOC == 'Debit') {
            string=string+"<tr>\n" +
                "<td>"+array[i]._id+"</td>"+
                "<td>"+moment(array[i].date).format("DD/MM/YYYY")+"</td>\n" +
                "<td>"+array[i].Description+"</td>\n" +
                "<td>-"+array[i].Amount+"</td>\n" +
                "<td >"+array[i].DOC+"</td>\n" +
                "<td >"+array[i].Responsible+"</td>\n" +
                "<td >"+array[i].Category+"</td>\n" +
                "</tr>\n";
            debit += parseInt(array[i].Amount,10);
        }
        else {
            string=string+"<tr>\n" +
                "<td>"+array[i]._id+"</td>"+
                "<td >"+moment(array[i].date).format("DD/MM/YYYY")+"</td>\n" +
                "<td >"+array[i].Description+"</td>\n" +
                "<td >"+array[i].Amount+"</td>\n" +
                "<td >"+array[i].DOC+"</td>\n" +
                "<td >"+array[i].Responsible+"</td>\n" +
                "<td >"+array[i].Category+"</td>\n" +
                "</tr>\n";
            credit += parseInt(array[i].Amount,10);
        }
    }
    Balance = credit - debit;
    return new Handlebars.SafeString(string);
});

Handlebars.registerHelper('DetailsRow',function (array) {
    string="";
    var debit=0,credit=0;
    for (var i = 0; i < array.length; i++) {
        if (array[i].DOC == 'Debit') {
            string=string+"<tr>\n" +
                "<td></td>"+
                "<td class='click id'>"+array[i]._id+"</td>"+
                "<td class='click'>"+moment(array[i].date).format("DD/MM/YYYY")+"</td>\n" +
                "<td class='click'>"+array[i].Description+"</td>\n" +
                "<td class='click'>"+array[i].DOC+"</td>\n" +
                "<td class='click'>-"+array[i].Amount+"</td>\n" +
                "</tr>\n";
            debit += parseInt(array[i].Amount,10);
        }
        else {
            string=string+"<tr>\n" +
                "<td></td>"+
                "<td class='click id'>"+array[i]._id+"</td>"+
                "<td class='click'>"+moment(array[i].date).format("DD/MM/YYYY")+"</td>\n" +
                "<td class='click'>"+array[i].Description+"</td>\n" +
                "<td class='click'>"+array[i].DOC+"</td>\n" +
                "<td class='click'>"+array[i].Amount+"</td>\n" +
                "</tr>\n";
            credit += parseInt(array[i].Amount,10);
        }
    }
    Balance = credit - debit;
    return new Handlebars.SafeString(string);
});



Handlebars.registerHelper('ShowBalance',function () {
    string="<div class='pull-right' style='padding-right: 15%'><h3>Total Amount:  &nbsp;"+Balance+"</h3>"+
        "</div>";
    return new Handlebars.SafeString(string);
});
Handlebars.registerHelper('ShowBalance2',function () {
    string="Total:"+Balance;
    return new Handlebars.SafeString(string);
});

