var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var hbs =require('express-handlebars');
var logger = require('morgan');
var multer = require('multer');

var usersRouter = require('./routes/users');

var app = express();
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');

var morgan       = require('morgan');


var session      = require('express-session');

var configDB = require('./config/database.js');

mongoose.connect(configDB.url);
require('./config/passport')(passport);

app.engine('hbs',hbs({extname:'hbs',defaultLayout:'layout',layoutsDir:__dirname+'/views/layouts/'}));
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');


app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms




app.use(logger('dev'));
app.use(express.json());

app.use(express.urlencoded({ extended: false }));
app.use(session({
    path: '/',
    secret: 'ilovescotchscotchyscotchscotch',
    saveUninitialized: false,
    resave: true,
    rolling: true,
    cookie: { maxAge : 3600000 }
})); // session
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(express.static(path.join(__dirname, 'public')));
var indexRouter = require('./routes/index')(app,passport);

app.use('/users', usersRouter);



//app.use('/', indexRouter);// load our routes and pass in our app and fully configured passport
// catch 404 and forward to error handler
app.use(function(req, res, next) {
    next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});




module.exports = app;
